# POC

To initialize your coin you need some tools:

 - [Truffle](http://truffleframework.com)
 - [TestRPC](https://github.com/ethereumjs/testrpc)
 - [Metamask](https://metamask.io/)


# Get start

First run TestRPC

`testrpc -m "your secret words from metamask" -u 0 -u 0`

`truffle compile`
`truffle migrate`

# Update contract

```
truffle console

compile
migrate
HelloWorld.new().then(function(res) { sc = HelloWorld.at(res.address); console.log(sc.address); });
```


#Troubleshoot

Sometime TestRPC can crash. To restart it just `killall node` and then start TestRPC again.
