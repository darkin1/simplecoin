// Import the page's CSS. Webpack will know what to do with it.
import "../stylesheets/app.css";

// Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'


// Import our contract artifacts and turn them into usable abstractions.
import helloWorld_artifacts from '../../build/contracts/HelloWorld.json'

// MetaCoin is our usable abstraction, which we'll use through the code below.
var SimpleCoin = contract(helloWorld_artifacts);

// var SimpleCoinX = web3.eth.contract(helloWorld_artifacts).at('0xcecd29af088a28fd7a509916f18ce21b511c8a93');

// The following code is simple to show off interacting with your contracts.
// As your needs grow you will likely need to change its form and structure.
// For application bootstrapping, check out window.addEventListener below.
var accounts;
var account;

/**
(0) 0x30a6b5dc2d976725afeefdb22f581ea8223b0bfd
(1) 0x8a3288231b6ce844290dae112104a32bac8c6e50
(2) 0x616926cddc8f697e09e783ffda21234ed4cb3adf
(3) 0xbde8b8178e2d57f8e3d1576e1c1043fcf4c5847d
(4) 0xb976c5a1f773ed29d9574e0f2c88dd0007380d63
(5) 0xbc137a03072145819fd9d4ccd3a12255222bd136
(6) 0x44166c1d183780d3864440d5a383205713176df5
(7) 0x9f3a67d99bdd9d5e1f34290427ecf30b4d80f3a7
(8) 0x007c786c4e457ee9446c70161f1460963b8b6337
(9) 0x522ecc9ebbc5a74e8a14d467e5a2335d0222c174
*/

window.App = {
  sender: "0x30A6b5Dc2D976725afEefDB22F581Ea8223b0bfD",
  receiver: "0x8A3288231B6cE844290dAE112104a32bAC8c6E50",
  contractAddress: "0xcecd29af088a28fd7a509916f18ce21b511c8a93",
  //0xa4b62117c94a665bc15b7f00ec4281d94f64c1a8
  //0xcecd29af088a28fd7a509916f18ce21b511c8a93

  start: function() {
    var self = this;

    // Bootstrap the HelloWorldCoin abstraction for Use.
    var x = SimpleCoin.setProvider(web3.currentProvider);

    SimpleCoin.deployed().then(function(instance){
      console.log('Smart contract address',instance.address);
    });

    document.getElementById('sender-address').innerText = self.sender;
    document.getElementById('receiver-address').innerText = self.receiver;

    self.refreshBalance('sender-balance', self.sender);
    self.refreshBalance('receiver-balance', self.receiver);

    document.getElementById('send-coin').addEventListener('click', function(){
      self.sendCoin();
    });



    var SimpleCoinX = web3.eth.contract(helloWorld_artifacts.abi);
    var c = SimpleCoinX.at(self.contractAddress);

    c.Transfer(function(err, res){
      console.log('transfer web3',res);
    });

  },

  refreshBalance: function(whoEl, whoAddr) {
    var self = this;

    var meta;
    SimpleCoin.deployed().then(function(instance) {
      meta = instance;
      return meta.getBalance.call(whoAddr);
    }).then(function(value) {
      console.log('refreshBalance',value.toNumber());
      document.getElementById(whoEl).innerText = value.toNumber();
    }).catch(function(e) {
      console.log(e);
    });
  },


  sendCoin: function() {
    var self = this;


    var amount = 100;

    var meta;
    SimpleCoin.deployed().then(function(instance) {
      meta = instance;
      return meta.transferX(self.receiver, amount, {from: self.sender});
    }).then(function() {
      self.refreshBalance('sender-balance', self.sender);
      self.refreshBalance('receiver-balance', self.receiver);
      // self.refreshBalance();
    }).catch(function(e) {
      console.log(e);
    });
  }


};

window.addEventListener('load', function() {
  // window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source. If you find that your accounts don't appear or you have 0 MetaCoin, ensure you've configured that source properly. If using MetaMask, see the following link. Feel free to delete this warning. :) http://truffleframework.com/tutorials/truffle-and-metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  }

  App.start();
});



// Checking if Web3 has been injected by the browser (Mist/MetaMask)
// if (typeof web3 !== 'undefined') {
//   console.warn("Using web3 detected from external source. If you find that your accounts don't appear or you have 0 MetaCoin, ensure you've configured that source properly. If using MetaMask, see the following link. Feel free to delete this warning. :) http://truffleframework.com/tutorials/truffle-and-metamask")
//   // Use Mist/MetaMask's provider
//   window.web3 = new Web3(web3.currentProvider);
// } else {
//   console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
//   // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
//   window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
// }


// Get the initial account balance so it can be displayed.
// web3.eth.getAccounts(function(err, accs) {
//   if (err != null) {
//     alert("There was an error fetching your accounts.");
//     return;
//   }
//
//   if (accs.length == 0) {
//     alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
//     return;
//   }
//
//   accounts = accs;
//   account = accounts[0];
//
//   console.log('accounts',accounts);
// });



// var filter = web3.eth.filter({
//   fromBlock: 10,
// });
//
// filter.watch(function(error, result){
//   if (error) {
//     console.error('filter error', error);
//   }
//
//   console.log('transaction',result);
//
// });
