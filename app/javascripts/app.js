import { default as Web3} from 'web3';
import SimpleCoinArtifacts from '../../build/contracts/HelloWorld.json'
var SmartContractAddress = '0x987a37e6e467e222bb32e495fbc1dfe1571e41bd';

window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

web3.eth.defaultAccount = '0x522ecC9eBbc5A74E8a14D467E5A2335D0222C174';

var SimpleCoin = web3.eth.contract(SimpleCoinArtifacts.abi);
var contract = SimpleCoin.at(SmartContractAddress);

window.App = {

  //1) 0x30A6b5Dc2D976725afEefDB22F581Ea8223b0bfD  Account 1
  //2) 0x8A3288231B6cE844290dAE112104a32bAC8c6E50  Account 2
  //3) 0x616926cdDc8F697e09E783FFdA21234ED4CB3aDf  Account 3
  //9) 0x522ecC9eBbc5A74E8a14D467E5A2335D0222C174  Account 9 (loose ?)
  sender: contract._eth.coinbase, //"0x30A6b5Dc2D976725afEefDB22F581Ea8223b0bfD",
  receiver: "0x8A3288231B6cE844290dAE112104a32bAC8c6E50",
  contractAddress: SmartContractAddress,
  amount: 30,

  start: function() {
    console.log('Sender address',contract._eth.coinbase);

    this.refreshSenderEthBalance();
    this.refreshSenderBalance();
    this.refreshReceiverBalance();

    this.transferEventListener();

    document.getElementById('sender-address').innerText = this.sender;
    document.getElementById('receiver-address').innerText = this.receiver;
    document.getElementById('send-coin').addEventListener('click', () => {
      this.sendCoin();
    });


    this.whiteListFunction(this.receiver);

    console.log('version: ', contract.getVersion() );
  },

  // totalTokens: function() {
  //   contract._eth.
  // },

  //That same as ETH balance from MetaMask
  refreshSenderEthBalance: function() {
    var balance = contract._eth.getBalance(this.sender, (err, bal) => {
      document.getElementById('sender-eth-balance').innerText = web3.fromWei(bal, 'ether').toNumber();
      // console.log('Sender ETH balance', web3.fromWei(bal, 'ether').toNumber() ,bal.toNumber());
    });
  },

  refreshSenderBalance: function() {
    // var balance = contract.getBalance.call(this.sender);
    // console.log(balance.toNumber());

    contract.getBalance(this.sender, (err, bal) => {
      document.getElementById('sender-balance').innerText = web3.toDecimal(bal);
      // console.log(web3.toDecimal(bal) );
      // console.log(web3.fromWei(bal, 'ether').toNumber() );
      // console.log(bal);
    });
  },

  refreshReceiverBalance: function() {
    contract.getBalance(this.receiver, (err, bal) => {
      document.getElementById('receiver-balance').innerText = web3.toDecimal(bal);
    });
    // var balance = contract.getBalance.call(this.receiver);
    // console.log(balance.toNumber());
  },

  sendCoin: function() {

    contract.transferX(this.receiver, this.amount, {from: this.sender});

    this.refreshSenderEthBalance();
    this.refreshSenderBalance();
    this.refreshReceiverBalance();
  },

  transferEventListener: function() {
    var trasferLogsEl = document.getElementById('transfer-logs')

    contract.Transfer(function(err, res){
      // console.log('transfer web3',res);

      var html = [
        '<p>' + res.args._from + ' => ' + res.args._to + ' = <strong>' + res.args._value + '</strong></p>',
        '<p> Extra data: <strong>' + web3.toAscii(res.args._extraData) + '</strong></p><br/>'
      ].join('');
      trasferLogsEl.insertAdjacentHTML('afterbegin', html);
    });
  },

  whiteListFunction: function(addr) {

    var user = {
      name: 'Jan2xxx',
      surname: 'Kowalskixxxx',
      age: 30,
    }
    var serialized = JSON.stringify(user);

    console.log('serialized: ', serialized);

    contract.addToWhiteList(addr, serialized, function(err, a ){console.log('addToWhiteList',a)});

    contract.checkUserInWhiteList(addr, function(err, res){
      console.log('checkUserInWhiteList', res);
    });

    contract.getUserFromWhiteList(addr, function(err, res){

      var object = JSON.parse(res[0]);

      var html = [
        '<p>name: '+ object.name +'</p>',
        '<p>name: '+ object.surname +'</p>',
        '<p>name: '+ object.age +'</p>',
      ].join('');
      document.getElementById('blockchain-logs').insertAdjacentHTML('afterbegin', html);

      // console.log('getUserFromWhiteList', res[0], res[1].toNumber(), res[2]);
    });

    console.log(web3.eth.estimateGas({from: '0x522ecc9ebbc5a74e8a14d467e5a2335d0222c174', to:"0x987a37e6e467e222bb32e495fbc1dfe1571e41bd", data: "0xcaaeb7170000000000000000000000008a3288231b6ce844290dae112104a32bac8c6e500000000000000000000000000000000000000000000000000000000000000040000000000000000000000000000000000000000000000000000000000000002d7b226e616d65223a224a616e32222c227375726e616d65223a224b6f77616c736b69222c22616765223a33307d00000000000000000000000000000000000000"}))

  }

};



window.addEventListener('load', function() {

  // if (typeof web3 !== 'undefined') {
  //   console.warn("Web3.js injected via MetaMask (or similar tool). More info here: http://truffleframework.com/tutorials/truffle-and-metamask")
  //   window.web3 = new Web3(web3.currentProvider);
  // } else {
  //   console.warn("You use pure web3.js. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
  //   window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  // }

  App.start();
});


//"0x8A3288231B6cE844290dAE112104a32bAC8c6E50", "{\"name\":\"Jan2\",\"surname\":\"Kowalski\",\"age\":30}"
