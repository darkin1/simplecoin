pragma solidity ^0.4.14;

contract HelloWorld {

  string public version = 'v0.0.2';
  address public owner;
  mapping (address => uint) public balances;
  struct User {
    string name;
    uint balance;
    bool verified;
  }
  mapping (address => User) public whiteList;
  /*User[] public blackList;*/
  event Transfer(address indexed _from, address indexed _to, uint256 _value, bytes _extraData);


  function HelloWorld() public {
    owner = msg.sender;
    balances[owner] = 10000;
  }

  function getVersion() constant public returns (string) {
    return version;
  }

  /// @notice Send tokens to another user
  /// @param receiver Send tokens to receiver
  /// @param amount Amount of tokens to send
  function transferX(address receiver, uint amount) public returns (bool success) {

    if(balances[msg.sender] < amount) {
      return false;
    }

    balances[msg.sender] -= amount;
    balances[receiver] += amount;

    Transfer(msg.sender, receiver, amount, "Some cool text");
    //"Secret USER ID: " -- works
    //"Secret USER ID: "+msg.sender -- is hard to achieve also is not recommended
    //sha256("Secret USER ID: ") -- works
    //sha256(msg.sender) -- works
    //s(1111, 2222) - as a struct doesn't work

    return true;
  }

  function getBalance(address _user) constant public returns (uint balance) {
    return balances[_user];
  }

  function addToWhiteList(address _user, string _name) public returns (bool) {

    whiteList[_user] = User({name: _name, balance: 0, verified: true});

    return true;
  }

  function checkUserInWhiteList(address _user) constant public returns (bool) {
    if(!whiteList[_user].verified) {
      return false;
    }
    return true;
  }

  function getUserFromWhiteList(address _user) constant public returns (string, uint, bool) {
    return (whiteList[_user].name, whiteList[_user].balance, whiteList[_user].verified);
  }



  /*function addToBlackList(address _user, string _name) public returns (bool) {
    require(blackList.length != 0);
    uint256 last = blackList.length;

    blackList[last] = User({addr: _user, name: _name, balance: 0, verified: true});

    return true;
  }

  function checkUserInBlackList(address _user) constant public returns (bool) {

    for(uint i=1; i<blackList.length; i++) {
        if(blackList[i].addr == _user) {
          return true;
        }
    }

    return false;
  }*/

}


//web3.eth.accounts
//web3.eth.getBalance(web3.eth.accounts[0])

//owner: 0xca35b7d915458ef540ade6068dfe2f44e8fa733c
//account[1]: 0x30A6b5Dc2D976725afEefDB22F581Ea8223b0bfD

//0xe9809ecbfa20d0054196d62ff1eb0460ef8c133182a75a40fc9ca2d15e87bc2b


//truffle console
//compile
//migrate
//HelloWorld.new().then(function(res) { sc = HelloWorld.at(res.address) })
//sc.address
