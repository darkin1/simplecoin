var HelloWorld = artifacts.require("./HelloWorld.sol");

contract('HelloWorld', function(accounts) {

  it('should has 1000 on beginning', function() {
    return HelloWorld.deployed().then(function(instance) {
      return instance.getBalance.call(accounts[0]);
    }).then(function(balance){
      // console.log(balance);
        assert.equal(balance.valueOf(), 10000, "10000 wasn't in the first account");
    })
  });

  it('transfer 100 money', function(){
    var ins;

    var sender = accounts[0];
    var receiver = accounts[1];

    var senderBegginingBalance;
    var receiverBegginingBalance;
    var senderAfterTransferBalance;
    var receiverAfterTransferBalance;

    var amount = 100;

    HelloWorld.deployed()
    .then(function(instance){
      ins = instance;

      return instance.getBalance.call(sender);
    })
    .then(function(balance){
      senderBegginingBalance = balance.toNumber();
      return ins.getBalance.call(receiver);
    })
    .then(function(balance){
      receiverBegginingBalance = balance.toNumber();
    })
    .then(function(){
      return ins.transferX(receiver, amount);
    })
    .then(function(){
      return ins.getBalance.call(sender);
    })
    .then(function(balance){
      senderAfterTransferBalance = balance.toNumber();

      return ins.getBalance.call(receiver);
    })
    .then(function(balance){
      receiverAfterTransferBalance = balance.toNumber();
    })
    .then(function(success){
      assert.equal(senderAfterTransferBalance, senderBegginingBalance - amount, "Amount wasn't correctly taken from the sender");
      assert.equal(receiverAfterTransferBalance, receiverBegginingBalance + amount, "Amount wasn't correctly sent to the receiver");

      // console.log('senderAfterTransferBalance', senderAfterTransferBalance);
      // console.log('receiverAfterTransferBalance', receiverAfterTransferBalance);
    });
  });
});
